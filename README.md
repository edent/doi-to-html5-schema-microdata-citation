# DOI to HTML5 Schema Microdata Citation

A small script to turn a DOI into a semantic HTML5 citation.  See https://shkspr.mobi/blog/2019/12/convert-doi-to-a-html5-schema-citation/ for details