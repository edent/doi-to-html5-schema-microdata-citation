# https://shkspr.mobi/blog/2019/12/convert-doi-to-a-html5-schema-citation/
import crossref_commons.retrieval
import os
import re

# Set a friendly header
os.environ['CR_API_MAILTO'] = 'crossref@shkspr.mobi'

reference = input("Enter a DOI: ")

# Check DOI is valid
# https://www.crossref.org/blog/dois-and-matching-regular-expressions/
regex = r"^10.\d{4,9}/[-._;()/:A-Z0-9]+$"
matches = re.search(regex, reference, re.IGNORECASE)

if matches:
	# Call the API
	data = crossref_commons.retrieval.get_publication_as_json(reference)

	# Start the citation
	citation_html = '<span itemscope itemtype="http://schema.org/ScholarlyArticle"><span itemprop="citation">'

	# Get all the authors
	authors = []
	for a in data["author"]:
		author_html = '<span itemprop="author" itemscope itemtype="http://schema.org/Person">'

		if "ORCID" in a:
			author_html += '<link itemprop="url" href="' + a["ORCID"] + '"/>'

		author_html += '<span itemprop="name"><span itemprop="familyName">' + a["family"] +'</span>, <span itemprop="givenName">' + a["given"] + '</span></span></span>'

		authors.extend({author_html})

	# Add the authors to the citation
	for author in authors:
		citation_html += author
		citation_html += " &amp; "
	# Remove the last &
	citation_html = citation_html.rstrip(' &amp;')

	# Title and any language information
	if "title" in data:
		headline  = data["title"][0]

		if "language" in data:
			lang = data["language"]
			citation_html += ' <q><cite itemprop="headline" lang="'+lang+'"><span itemprop="inLanguage" content="'+lang+'">'+headline+'</span></cite></q> '
		else:
			citation_html += ' <q><cite itemprop="headline">'+headline+'</cite></q> '

	# Add the date
	if "published-print" in data:
		year = data["published-print"]["date-parts"][0][0]
		citation_html += '(<time itemprop="datePublished" datetime="'+str(year)+'">' + str(year) + '</time>) '
	elif "issued" in data:
		year = data["issued"]["date-parts"][0][0]
		citation_html += '(<time itemprop="datePublished" datetime="'+str(year)+'">' + str(year) + '</time>) '
	elif "created" in data:
		datetime = data["created"]["date-time"]
		year     = data["created"]["date-parts"][0][0]
		citation_html += '(<time itemprop="datePublished" datetime="'+datetime+'">' + str(year) + '</time>) '
	elif "deposited" in data:
		datetime = data["deposited"]["date-time"]
		year     = data["deposited"]["date-parts"][0][0]
		citation_html += '(<time itemprop="datePublished" datetime="'+datetime+'">' + str(year) + '</time>) '

	# Page number information
	if "page" in data:
		citation_html += ' page: <span itemprop="pagination">' + data["page"] + '</span>. '

	# Publisher
	if "publisher" in data:
		citation_html += '<span itemprop="publisher" itemscope itemtype="http://schema.org/Organization"><span itemprop="name">'+data["publisher"]+'</span></span>. '

	# Publication
	if "container-title" in data:
		citation_html += '<span itemprop="publication">'+data["container-title"][0]+'</span>. '

	# DOI link
	if "DOI" in data:
		doi = data["DOI"]
		doi_url = "https://doi.org/" + doi
		citation_html += '<a itemprop="url" href="'+doi_url+'">'+doi_url+'</a>'

	# End the citation
	citation_html += '</span></span>'

	print(citation_html)
